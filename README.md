# Voiting Contract
This repository contains a dApp for voting on some proposals.

Repository contains the following directories:
- back
- docsAssets

docsAssets - image folder for readme.md files.

The back directory structure looks like this:
- `/contracts/test/`- contracts used for tests.
- `/contracts/balancer`- Balancer Pool smart contract.
- `/contracts/gnosis`- Gnosis Safe smart conract.
- `/contracts/erc20`- mock for ERC20 token.
- `/deploy`- contracts deployment script.
- `/test`- unit and happy path simulation tests for contracts.
- `/test/helpers`- common code for test. Token generation and initial data, for example.

## Development

requires

```
node >= 12.0
```

to install node modules

```
cd back
npm i
```

to test

```
cd back
npm run test --parallel
```

to run tests with results output to the console to demonstrate work

```
cd back
npx hardhat test .\test\voiting-simulation-happypath.js
```

to run coverage

```
cd back
npm run coverage
```

## Deployment

This project uses the hardhat-deploy plugin to deploy contracts. When a contract has been deployed, it is saved as JSON to the `./build/artifacts/contracts` directory, including its _address_ as well as its _abi_.

### Deployment to local net

```
npx hardhat node
```

## Code formatting

To format JS and Solidity code, run the following command:

`npm run format`

## Proof of work in the absence of a user interface
This section is added to show that voting flow фтв interaction with gnosis works correct.
This demo is made as a test that prints data to the console.

To run this flow you need to execute command:
```
cd back
npx hardhat test .\test\voting-simulation-with-console-output.js
```

1. Displaying the votes count for initial proposals
![initProposals](./docsAssets/initialProposals.png)
2. Displaying the gnosis initial owner
![initProposals](./docsAssets/initialGnosis.png)
3. Output of users and their votes
![initProposals](./docsAssets/votingProcess.png)
4. Displaying proposals vote count after voitiing
![initProposals](./docsAssets/voteCount.png)
5. Close voting
![initProposals](./docsAssets/closeVoting.png)
6. Show winnig proposal info
![initProposals](./docsAssets/winnigProposal.png)
7. Show that owner from winning proposal added to gnosis
![initProposals](./docsAssets/ownersInGnosis.png)