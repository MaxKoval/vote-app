const deployFunction = async ({ getNamedAccounts, deployments, ethers }) => {
  const { deploy } = deployments;
  const { root } = await getNamedAccounts();

  const voteDeploy = await deploy("Vote", {
    from: root,
    args: [],
    log: true,
  });

  console.log(`Root address is ${root}`);
  console.log(`Vote contract address is ${voteDeploy.address}`);
};

module.exports = deployFunction;
module.exports.tags = ["Vote"];
