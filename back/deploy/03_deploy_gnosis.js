const deployFunction = async ({ getNamedAccounts, deployments, ethers }) => {
  const { deploy } = deployments;
  const { root, gnosisSafeOwner } = await getNamedAccounts();

  const gnosisDeploy = await deploy("GnosisOwnerManager", {
    contract: "contracts/gnosis/GnosisOwnerManager.sol:GnosisOwnerManager",
    from: gnosisSafeOwner,
    log: true,
  });

  const gnosisSafe = await ethers.getContractAt(
    "contracts/gnosis/GnosisOwnerManager.sol:GnosisOwnerManager",
    gnosisDeploy.address
  );

  await gnosisSafe.setupOwners([root], 1);

  console.log(`Gnosis safe address is ${gnosisDeploy.address}`);
};

module.exports = deployFunction;
module.exports.tags = ["GnosisSafe"];
