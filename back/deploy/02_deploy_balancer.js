const ConfigurableRightsPool = artifacts.require('ConfigurableRightsPool');
const BFactory = artifacts.require('BFactory');
const CRPFactory = artifacts.require('CRPFactory');
const BalancerSafeMath = artifacts.require('BalancerSafeMath');
const RightsManager = artifacts.require('RightsManager');
const SmartPoolManager = artifacts.require('SmartPoolManager');

const { toWei } = web3.utils;
const MAX = web3.utils.toTwosComplement(-1);

const deployFunction = async ({
  ethers
}) => {
  const erc20Factory = await ethers.getContractFactory('ERC20Mock');

  const tokens = {
    dai: await erc20Factory.deploy('DAI Stablecoin', 'DAI'),
    usdc: await erc20Factory.deploy('USDC Stablecoin', 'USDC'),
  }

  const bfactory = await BFactory.new();
  const balancerSafeMath = await BalancerSafeMath.new();
  const rightsManager = await RightsManager.new();
  const smartPoolManager = await SmartPoolManager.new();

  await CRPFactory.link(balancerSafeMath, balancerSafeMath.address);
  await CRPFactory.link(rightsManager, rightsManager.address);
  await CRPFactory.link(smartPoolManager, smartPoolManager.address);

  const crpFactory = await CRPFactory.new();

  const dai = await tokens.dai;
  const usdc = await tokens.usdc;

  const USDC = await usdc.address;
  const DAI = await dai.address;

  const tokenAddresses = [DAI, USDC];

  const swapFee = 10 ** 15;
  const startWeights = [toWei("8"), toWei("1")];
  const startBalances = [toWei("10000"), toWei("5000")];
  const SYMBOL = "BPOOL";
  const NAME = "Prime Balancer Pool Token";

  const permissions = {
    canPauseSwapping: true,
    canChangeSwapFee: true,
    canChangeWeights: true,
    canAddRemoveTokens: true,
    canWhitelistLPs: false,
  };

  const poolParams = {
    poolTokenSymbol: SYMBOL,
    poolTokenName: NAME,
    constituentTokens: tokenAddresses,
    tokenBalances: startBalances,
    tokenWeights: startWeights,
    swapFee: swapFee,
  };

  POOL = await crpFactory.newCrp.call(
    bfactory.address,
    poolParams,
    permissions
  );

  await crpFactory.newCrp(bfactory.address, poolParams, permissions);

  const pool = await ConfigurableRightsPool.at(POOL);

  await usdc.approve(POOL, MAX);
  await dai.approve(POOL, MAX);

  await pool.createPool(toWei("1000"), 10, 10);

  console.log(`Balancer pool address is ${pool.address}`);
};

module.exports = deployFunction;
module.exports.tags = ["BalancerPool"];
