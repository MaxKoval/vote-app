// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.6;
pragma experimental ABIEncoderV2;

import "hardhat/console.sol";
import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";
import "../gnosis/GnosisOwnerManager.sol";

contract Vote {
    struct Voter {
        uint256 weight; // vote weight based on balancer pool share
        bool isVoted;
        bool available;
    }

    struct Proposal {
        uint256 totalVoteWeight; // the sum of the weights of those who voted for propopsal
        string commandName; // the command to be executed when the poposal is won
        address ownerAddress; // the address of the owner to be added or removed from gnosis
    }

    bool public isInitialized; // shows if the contract is initialized or not
    bool public isVoteOpen = true;

    address public owner;

    IERC20 public balancerPool;
    GnosisOwnerManager public gnosis;

    mapping(address => Voter) private voters;

    Proposal[] public winningProposals;
    Proposal[] public proposals;

    string private constant CREATE_OWNER_COMMAND = "createOwner";
    string private constant REMOVE_OWNER_COMMAND = "removeOwner";

    modifier initializer() {
        require(!isInitialized, "Vote: contract already initialized");
        isInitialized = true;
        _;
    }

    modifier onlyOwner() {
        require(owner == msg.sender, "Vote: person isn't admin");
        _;
    }

    modifier voteIsOpen() {
        require(isVoteOpen, "Vote: vote is closed");
        _;
    }

    constructor() {
        owner = msg.sender;
    }

    /**
     * @dev                           Initialize Vote contract
     * @param _initialPoposals        Proposals for voting
     * @param _balancerPoolAddress    Address of balancer pool
     * @param _gnosisAddress          Address of Gnosis Safe
     */
    function initialize(
        Proposal[] calldata _initialPoposals,
        address _balancerPoolAddress,
        address _gnosisAddress
    ) external initializer onlyOwner {
        require(
            _balancerPoolAddress != address(0),
            "Vote: can't initialize a contract without balancer pool address"
        );

        require(
            _gnosisAddress != address(0),
            "Vote: can't initialize a contract without gnosis safe address"
        );

        balancerPool = IERC20(_balancerPoolAddress);
        gnosis = GnosisOwnerManager(_gnosisAddress);

        validateProposals(_initialPoposals);

        for (uint256 i = 0; i < _initialPoposals.length; i++) {
            proposals.push(_initialPoposals[i]);
        }
    }

    /**
     * @dev                     Method to vote for proposal
     * @param _proposalIndex    Index of selected proposal in array
     */
    function vote(uint256 _proposalIndex) external voteIsOpen {
        Voter storage voter = voters[msg.sender];

        // Check if there is a voter in the array of voters, if not, then add
        if (!voter.available) {
            voters[msg.sender] = Voter({
                weight: balancerPool.balanceOf(msg.sender),
                isVoted: false,
                available: true
            });
        }

        require(voter.weight > 0 && !voter.isVoted, "Vote: person can't vote");

        // adding vote weight
        proposals[_proposalIndex].totalVoteWeight += voter.weight;

        voter.isVoted = true;
    }

    function closeVoting() external onlyOwner voteIsOpen {
        isVoteOpen = false;

        calculateWinningProposals();
        executeWinningProposalCommand();

        delete proposals;
    }

    function addProposals(Proposal[] calldata _newProposals)
        external
        onlyOwner
    {
        delete winningProposals;

        validateProposals(_newProposals);

        for (uint256 i = 0; i < _newProposals.length; i++) {
            proposals.push(_newProposals[i]);
        }
    }

    function getProposals() external view returns (Proposal[] memory) {
        return proposals;
    }

    function getWinningProposals() external view returns (Proposal[] memory) {
        return winningProposals;
    }

    function calculateWinningProposals() private {
        uint256 winningTotalVoteWeight = 0;

        for (uint256 i = 0; i < proposals.length; i++) {
            if (proposals[i].totalVoteWeight == winningTotalVoteWeight) {
                winningProposals.push(proposals[i]);
            }

            if (proposals[i].totalVoteWeight > winningTotalVoteWeight) {
                delete winningProposals;
                winningTotalVoteWeight = proposals[i].totalVoteWeight;
                winningProposals.push(proposals[i]);
            }
        }
    }

    function executeWinningProposalCommand() private {
        for (uint256 i = 0; i < winningProposals.length; i++) {
            if (isCreateOwnerCommand(winningProposals[i].commandName)) {
                gnosis.addOwnerWithThreshold(
                    winningProposals[i].ownerAddress,
                    1
                );
            } else {
                address prevOwnerAddress = getPrevOwnerAddress(
                    winningProposals[i].ownerAddress
                );

                gnosis.removeOwner(
                    prevOwnerAddress,
                    winningProposals[i].ownerAddress,
                    1
                );
            }
        }
    }

    function getPrevOwnerAddress(address _ownerAddress)
        public
        view
        returns (address prevOwnerAddress)
    {
        address[] memory gnosisOwners = gnosis.getOwners();

        for (uint256 i = 0; i < gnosisOwners.length; i++) {
            if (_ownerAddress == gnosisOwners[0]) {
                return address(0x1);
            }

            if (_ownerAddress == gnosisOwners[i]) {
                return gnosisOwners[i + 1];
            }
        }
    }

    function validateProposals(Proposal[] memory newProposals) private view {
        require(
            newProposals.length > 0,
            "Vote: can't initialize a contract without proposals"
        );

        address[] memory gnosisOwners = gnosis.getOwners();
        uint256 removeOwnerProposalsCount = 0;

        for (uint256 i = 0; i < newProposals.length; i++) {
            require(
                bytes(newProposals[i].commandName).length > 0,
                "Vote: proposal must contain a command"
            );

            require(
                address(newProposals[i].ownerAddress) != address(0),
                "Vote: proposal must contain a owner address"
            );

            // find proposals duplicates
            for (uint256 j = i + 1; j < newProposals.length; j++) {
                if (
                    newProposals[i].ownerAddress == newProposals[j].ownerAddress
                ) {
                    bool isContainsDuplicateProposals = true;

                    require(
                        !isContainsDuplicateProposals,
                        "Vote: can't initialize a contract with duplicates proposals"
                    );
                }
            }

            bool isOwner = gnosis.isOwner(newProposals[i].ownerAddress);

            // Checking if the list of props contains a command to add an existing owner
            if (isCreateOwnerCommand(newProposals[i].commandName)) {
                require(!isOwner, "Vote: owner already exists in gnosis");
            }

            // Checking if the list of props contains a command to remove an unexisting owner
            if (isRemoveOwnerCommand(newProposals[i].commandName)) {
                require(isOwner, "Vote: owner doesn't exist in gnosis");
                removeOwnerProposalsCount++;
            }
        }

        // Gnosis must have at least one owner
        if (removeOwnerProposalsCount > 0) {
            require(
                gnosisOwners.length > 1,
                "Vote: nobody can be removed. Gnosis has one owner"
            );
        }
    }

    function isCreateOwnerCommand(string memory commandName)
        private
        pure
        returns (bool)
    {
        return
            keccak256(abi.encodePacked(commandName)) ==
            keccak256(abi.encodePacked(CREATE_OWNER_COMMAND));
    }

    function isRemoveOwnerCommand(string memory commandName)
        private
        pure
        returns (bool)
    {
        return
            keccak256(abi.encodePacked(commandName)) ==
            keccak256(abi.encodePacked(REMOVE_OWNER_COMMAND));
    }
}
