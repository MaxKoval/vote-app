const { initialize, createGnosis, createBalancer } = require("./helpers/setup");
const { toWei } = require("web3-utils");

const deploy = async () => {
  const setup = await initialize();

  setup.gnosis = await createGnosis(setup.accounts[0].address, 1);
  setup.pool = await createBalancer(setup.tokens);

  return setup;
};

describe("Vote contract", function () {
  let voteContractDeploy;
  let voterWith0OnBalance;
  let voterWith10OnBalance;
  let voterWith15OnBalance;
  let voterWith25OnBalance;
  let voterWith45OnBalance;
  let proposals;
  let pool;
  let gnosis;

  before(async () => {
    const setup = await deploy();

    proposals = setup.proposals;
    
    [
      admin,
      voterWith0OnBalance,
      voterWith10OnBalance,
      voterWith15OnBalance,
      voterWith25OnBalance,
      voterWith45OnBalance,
    ] = setup.accounts;

    const voteContract = await ethers.getContractFactory("Vote");
    voteContractDeploy = await voteContract.deploy();

    gnosis = setup.gnosis;
    pool = setup.pool;

    await voteContractDeploy.initialize(
      setup.proposals,
      pool.address,
      gnosis.address
    );

    await pool.transfer(voterWith0OnBalance.address, toWei("0"));
    await pool.transfer(voterWith10OnBalance.address, toWei("10"));
    await pool.transfer(voterWith15OnBalance.address, toWei("15"));
    await pool.transfer(voterWith25OnBalance.address, toWei("25"));
    await pool.transfer(voterWith45OnBalance.address, toWei("45"));
  });

  context(
    "happypath: vote => end of voting => exec proposal command",
    async () => {
      it("voters balances are correct", async () => {
        expect(
          (await pool.balanceOf(voterWith0OnBalance.address)).toString()
        ).to.equal(toWei("0"));
        expect(
          (await pool.balanceOf(voterWith10OnBalance.address)).toString()
        ).to.equal(toWei("10"));
        expect(
          (await pool.balanceOf(voterWith15OnBalance.address)).toString()
        ).to.equal(toWei("15"));
        expect(
          (await pool.balanceOf(voterWith25OnBalance.address)).toString()
        ).to.equal(toWei("25"));
        expect(
          (await pool.balanceOf(voterWith45OnBalance.address)).toString()
        ).to.equal(toWei("45"));
      });

      it("proposal vote weights change correctly", async () => {
        await voteContractDeploy
          .connect(voterWith10OnBalance)
          .vote(proposals.indexOf(proposals[0]));
        await voteContractDeploy
          .connect(voterWith15OnBalance)
          .vote(proposals.indexOf(proposals[0]));
        await voteContractDeploy
          .connect(voterWith45OnBalance)
          .vote(proposals.indexOf(proposals[1]));

        expect(
          (await voteContractDeploy.getProposals())[0].totalVoteWeight
        ).to.equal(toWei("25"));
        expect(
          (await voteContractDeploy.getProposals())[1].totalVoteWeight
        ).to.equal(toWei("45"));
      });

      it("successfully close voting", async () => {
        await voteContractDeploy.closeVoting();

        expect(await voteContractDeploy.isVoteOpen()).to.equal(false);
      });

      it("win second proposal", async () => {
        expect(
          (await voteContractDeploy.getWinningProposals())[0].totalVoteWeight
        ).to.equal(toWei("45"));
      });

      it("command from proposal execute succefully", async () => {
        expect(await gnosis.getOwners()).to.contains(proposals[1].ownerAddress);
      });
    }
  );
});
