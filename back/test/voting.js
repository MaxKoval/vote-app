const { expect } = require("chai");
const { expectRevert } = require("@openzeppelin/test-helpers");
const { initialize, createGnosis, createBalancer } = require("./helpers/setup");
const { toWei } = require("web3-utils");

const deploy = async () => {
  const setup = await initialize();

  setup.gnosis = await createGnosis(setup.accounts[0].address, 1);
  setup.pool = await createBalancer(setup.tokens);

  return setup;
};

describe("Contract: Vote", function () {
  let voteContract;
  let voteContractDeploy;
  let accounts;
  let admin;
  let voterWith0OnBalance;
  let voterWith10OnBalance;
  let voterWith15OnBalance;
  let voterWith25OnBalance;
  let voterWith45OnBalance;
  let proposals;
  let additionalProposals;
  let proposalWithEmptyCommand;
  let proposalWithEmptyOwnerAddress;
  let duplacatesProposals;
  let proposalWithExistedOwner;
  let proposalWithUnexistedOwner;
  let proposalToRemoveOwner;
  let pool;

  before(async function () {
    const setup = await deploy();

    proposals = setup.proposals;
    baseProposals = setup.proposals.slice(0, 2);
    additionalProposals = setup.proposals.slice(
      setup.proposals.length - 2,
      setup.proposals.length
    );
    proposalWithEmptyName = setup.proposalWithEmptyName;
    proposalWithEmptyCommand = setup.proposalWithEmptyCommand;
    proposalWithEmptyOwnerAddress = setup.proposalWithEmptyOwnerAddress;
    proposalWithExistedOwner = setup.proposalWithExistedOwner;
    proposalWithUnexistedOwner = setup.proposalWithUnexistedOwner;
    proposalToRemoveOwner = setup.proposalToRemoveOwner;
    duplacatesProposals = setup.duplacatesProposals;

    accounts = setup.accounts;
    [
      admin,
      voterWith0OnBalance,
      voterWith10OnBalance,
      voterWith15OnBalance,
      voterWith25OnBalance,
      voterWith45OnBalance,
      voterWith25OnBalance1,
    ] = setup.accounts;

    gnosis = setup.gnosis;
    pool = setup.pool;

    await pool.transfer(voterWith0OnBalance.address, toWei("0"));
    await pool.transfer(voterWith10OnBalance.address, toWei("10"));
    await pool.transfer(voterWith15OnBalance.address, toWei("15"));
    await pool.transfer(voterWith25OnBalance.address, toWei("25"));
    await pool.transfer(voterWith45OnBalance.address, toWei("45"));
    await pool.transfer(voterWith25OnBalance1.address, toWei("25"));
  });

  context("» init", () => {
    beforeEach(async function () {
      voteContract = await ethers.getContractFactory("Vote");
      voteContractDeploy = await voteContract.deploy();
    });

    it("it initialized successfully", async function () {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      expect(await voteContractDeploy.isInitialized()).to.equal(true);
      expect(await voteContractDeploy.isVoteOpen()).to.equal(true);
      expect(await voteContractDeploy.owner()).to.equal(admin.address);
      expect((await voteContractDeploy.getWinningProposals()).length).to.equal(
        0
      );
      expect((await voteContractDeploy.getProposals()).length).to.equal(
        proposals.length
      );
    });

    it("it reverts on double initialization", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await expectRevert(
        voteContractDeploy.initialize(proposals, pool.address, gnosis.address),
        "Vote: contract already initialized"
      );
    });

    it("it reverts on empty proposals initialization", async () => {
      await expectRevert(
        voteContractDeploy.initialize([], pool.address, gnosis.address),
        "Vote: can't initialize a contract without proposals"
      );
    });

    // it("it reverts on empty proposal name", async () => {
    //   await expectRevert(
    //     voteContractDeploy.initialize(
    //       proposalWithEmptyName,
    //       pool.address,
    //       gnosis.address
    //     ),
    //     "Vote: proposal must contain a name"
    //   );
    // });

    it("it reverts on empty proposal command", async () => {
      await expectRevert(
        voteContractDeploy.initialize(
          proposalWithEmptyCommand,
          pool.address,
          gnosis.address
        ),
        "Vote: proposal must contain a command"
      );
    });

    it("it reverts on empty proposal owner address", async () => {
      await expectRevert(
        voteContractDeploy.initialize(
          proposalWithEmptyOwnerAddress,
          pool.address,
          gnosis.address
        ),
        "Vote: proposal must contain a owner address"
      );
    });

    it("it reverts on duplicates proposals", async () => {
      await expectRevert(
        voteContractDeploy.initialize(
          duplacatesProposals,
          pool.address,
          gnosis.address
        ),
        "Vote: can't initialize a contract with duplicates proposals"
      );
    });

    it("it reverts on proposals with create command of existing owner", async () => {
      await gnosis.addOwnerWithThreshold(accounts[9].address, 1);

      expect(accounts[9].address).to.equal(
        proposalWithExistedOwner[0].ownerAddress
      );

      await expectRevert(
        voteContractDeploy.initialize(
          proposalWithExistedOwner,
          pool.address,
          gnosis.address
        ),
        "Vote: owner already exists in gnosis"
      );
    });

    it("it reverts on proposals with remove command of unexisting owner", async () => {
      await expectRevert(
        voteContractDeploy.initialize(
          proposalWithUnexistedOwner,
          pool.address,
          gnosis.address
        ),
        "Vote: owner doesn't exist in gnosis"
      );
    });
  });

  context("» requirements", () => {
    beforeEach(async function () {
      voteContract = await ethers.getContractFactory("Vote");
      voteContractDeploy = await voteContract.deploy();

      gnosis = await createGnosis(admin.address, 1);
    });

    it("it prohibits a non-admin closing a vote", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await expectRevert(
        voteContractDeploy.connect(voterWith10OnBalance).closeVoting(),
        "Vote: person isn't admin"
      );

      expect(await voteContractDeploy.isVoteOpen()).to.equal(true);
    });

    it("it prohibits a non-admin create proposals", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await expectRevert(
        voteContractDeploy
          .connect(voterWith10OnBalance)
          .addProposals(additionalProposals),
        "Vote: person isn't admin"
      );
    });

    it("it allows only the admin to create new proposal", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy.addProposals(additionalProposals);

      expect((await voteContractDeploy.getProposals()).length).to.equal(
        proposals.length + additionalProposals.length
      );
    });

    it("it prohibit voting more than once", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith10OnBalance)
        .vote(proposals.indexOf(proposals[0]));

      await expectRevert(
        voteContractDeploy
          .connect(voterWith10OnBalance)
          .vote(proposals.indexOf(proposals[0])),
        "Vote: person can't vote"
      );
    });

    it("it prohibits a voter with zero weight to vote", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await expectRevert(
        voteContractDeploy
          .connect(voterWith0OnBalance)
          .vote(proposals.indexOf(proposals[0])),
        "Vote: person can't vote"
      );
    });

    it("it prohibits a non-admin closing a vote", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await expectRevert(
        voteContractDeploy.connect(voterWith10OnBalance).closeVoting(),
        "Vote: person isn't admin"
      );

      expect(await voteContractDeploy.isVoteOpen()).to.equal(true);
    });

    it("it allows only the admin to close the vote", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy.closeVoting();

      expect(await voteContractDeploy.isVoteOpen()).to.equal(false);
    });
  });

  context("» vote process", () => {
    beforeEach(async function () {
      voteContract = await ethers.getContractFactory("Vote");
      voteContractDeploy = await voteContract.deploy();

      gnosis = await createGnosis(admin.address, 1);
    });

    it("it correctly calculate votes for proposals", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith10OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith15OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith45OnBalance)
        .vote(proposals.indexOf(proposals[1]));

      expect(
        (await voteContractDeploy.getProposals())[0].totalVoteWeight
      ).to.equal(toWei("25"));
      expect(
        (await voteContractDeploy.getProposals())[1].totalVoteWeight
      ).to.equal(toWei("45"));
    });

    it("it return zero winning proposals, if voting isn't closed", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith10OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith15OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith45OnBalance)
        .vote(proposals.indexOf(proposals[1]));

      expect((await voteContractDeploy.getWinningProposals()).length).to.equal(
        0
      );
    });

    it("it prohibits voting if vote is closed", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith10OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy.closeVoting();

      await expectRevert(
        voteContractDeploy
          .connect(voterWith15OnBalance)
          .vote(proposals.indexOf(proposals[0])),
        "Vote: vote is closed"
      );
    });

    it("it return zero proposals, if voting is closed", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith10OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith15OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith45OnBalance)
        .vote(proposals.indexOf(proposals[1]));

      await voteContractDeploy.closeVoting();

      expect((await voteContractDeploy.getProposals()).length).to.equal(0);
    });

    it("win proposal with the highest vote weight", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith10OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith15OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith45OnBalance)
        .vote(proposals.indexOf(proposals[1]));

      await voteContractDeploy.closeVoting();

      expect(
        (await voteContractDeploy.getWinningProposals())[0].totalVoteWeight
      ).to.equal(toWei("45"));
    });

    it("more than one proposal can win in the vote", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith10OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith15OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith25OnBalance)
        .vote(proposals.indexOf(proposals[1]));

      await voteContractDeploy.closeVoting();

      expect(
        (await voteContractDeploy.getWinningProposals())[0].totalVoteWeight
      ).to.equal(toWei("25"));
      expect(
        (await voteContractDeploy.getWinningProposals())[1].totalVoteWeight
      ).to.equal(toWei("25"));
    });

    it("it succesfully create owner in gnosis from winning proposal", async () => {
      await voteContractDeploy.initialize(
        proposals,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith10OnBalance)
        .vote(proposals.indexOf(proposals[0]));
      await voteContractDeploy
        .connect(voterWith15OnBalance)
        .vote(proposals.indexOf(proposals[1]));

      await voteContractDeploy.closeVoting();

      expect(
        (await voteContractDeploy.getWinningProposals())[0].totalVoteWeight
      ).to.equal(toWei("15"));

      expect(await gnosis.getOwners()).to.contains(proposals[1].ownerAddress);
    });

    it("it succesfully remove owner in gnosis from winning proposal", async () => {
      await gnosis.addOwnerWithThreshold(
        proposalToRemoveOwner[0].ownerAddress,
        1
      );

      await voteContractDeploy.initialize(
        proposalToRemoveOwner,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith15OnBalance)
        .vote(proposalToRemoveOwner.indexOf(proposalToRemoveOwner[0]));

      await voteContractDeploy.closeVoting();

      expect(
        (await voteContractDeploy.getWinningProposals())[0].totalVoteWeight
      ).to.equal(toWei("15"));

      expect(await gnosis.getOwners()).to.not.contains(
        proposalToRemoveOwner[0].signerAddress
      );
    });

    it("it succesfully remove last owner", async () => {
      await gnosis.addOwnerWithThreshold(
        proposalToRemoveOwner[0].ownerAddress,
        1
      );

      await voteContractDeploy.initialize(
        proposalToRemoveOwner,
        pool.address,
        gnosis.address
      );

      await voteContractDeploy
        .connect(voterWith15OnBalance)
        .vote(proposalToRemoveOwner.indexOf(proposalToRemoveOwner[0]));

      await voteContractDeploy.closeVoting();

      expect(
        (await voteContractDeploy.getWinningProposals())[0].totalVoteWeight
      ).to.equal(toWei("15"));

      expect((await gnosis.getOwners())[0]).to.equal(admin.address);
    });

    it("it return zero address if want to delete last added owner", async () => {
      await gnosis.addOwnerWithThreshold(
        proposalToRemoveOwner[0].ownerAddress,
        1
      );

      await voteContractDeploy.initialize(
        proposalToRemoveOwner,
        pool.address,
        gnosis.address
      );

      expect(
        await voteContractDeploy.getPrevOwnerAddress(
          proposalToRemoveOwner[0].ownerAddress
        )
      ).to.equal("0x0000000000000000000000000000000000000001");
    });

    it("it return zero address if want to delete last added owner", async () => {
      await gnosis.addOwnerWithThreshold(proposals[0].ownerAddress, 1);
      await gnosis.addOwnerWithThreshold(proposals[1].ownerAddress, 1);
      await gnosis.addOwnerWithThreshold(proposals[2].ownerAddress, 1);

      await voteContractDeploy.initialize(
        [proposals[3]],
        pool.address,
        gnosis.address
      );

      expect(
        await voteContractDeploy.getPrevOwnerAddress(proposals[1].ownerAddress)
      ).to.equal(proposals[0].ownerAddress);
    });
  });
});
