const { initialize, createGnosis, createBalancer } = require("./helpers/setup");
const { toWei } = require("web3-utils");

const deploy = async () => {
  const setup = await initialize();

  setup.gnosis = await createGnosis(setup.accounts[0].address, 1);
  setup.pool = await createBalancer(setup.tokens);

  return setup;
};

describe("Vote contract", function () {
  let voteContractDeploy;
  let voterWith0OnBalance;
  let voterWith10OnBalance;
  let voterWith15OnBalance;
  let voterWith25OnBalance;
  let voterWith45OnBalance;
  let proposals;
  let pool;
  let gnosis;

  before(async () => {
    const setup = await deploy();

    proposals = setup.proposals;

    [
      admin,
      voterWith0OnBalance,
      voterWith10OnBalance,
      voterWith15OnBalance,
      voterWith25OnBalance,
      voterWith45OnBalance,
    ] = setup.accounts;

    const voteContract = await ethers.getContractFactory("Vote");
    voteContractDeploy = await voteContract.deploy();

    gnosis = setup.gnosis;
    pool = setup.pool;

    await voteContractDeploy.initialize(
      setup.proposals,
      pool.address,
      gnosis.address
    );

    await pool.transfer(voterWith0OnBalance.address, toWei("0"));
    await pool.transfer(voterWith10OnBalance.address, toWei("10"));
    await pool.transfer(voterWith15OnBalance.address, toWei("15"));
    await pool.transfer(voterWith25OnBalance.address, toWei("25"));
    await pool.transfer(voterWith45OnBalance.address, toWei("45"));
  });

  context(
    "happypath: vote => end of voting => exec proposal command",
    async () => {
      it("сonsole output", async () => {
        console.log("========================================================");
        console.log("Initial proposals vote count:");
        console.log(`- First: ${(await voteContractDeploy.getProposals())[0].totalVoteWeight}`);
        console.log(`- Second: ${(await voteContractDeploy.getProposals())[1].totalVoteWeight}`);
        console.log("========================================================");

        console.log("========================================================");
        console.log("Initial gnosis owner:");
        console.log(await gnosis.getOwners());
        console.log("========================================================");

        await voteContractDeploy
          .connect(voterWith10OnBalance)
          .vote(proposals.indexOf(proposals[0]));
        await voteContractDeploy
          .connect(voterWith15OnBalance)
          .vote(proposals.indexOf(proposals[0]));
        await voteContractDeploy
          .connect(voterWith45OnBalance)
          .vote(proposals.indexOf(proposals[1]));

        console.log("========================================================");
        console.log("Voting for proposals:");
        console.log(`Voter address: ${voterWith10OnBalance.address}. Weight: 10. Changed proposal: ${proposals.indexOf(proposals[0])}`);
        console.log(`Voter address: ${voterWith15OnBalance.address}. Weight: 15. Changed proposal: ${proposals.indexOf(proposals[0])}`);
        console.log(`Voter address: ${voterWith45OnBalance.address}. Weight: 45. Changed proposal: ${proposals.indexOf(proposals[1])}`);
        console.log("========================================================");

        console.log("========================================================");
        console.log("Proposals vote count after voitiing:");
        console.log(`- First: ${(await voteContractDeploy.getProposals())[0].totalVoteWeight}`);
        console.log(`- Second: ${(await voteContractDeploy.getProposals())[1].totalVoteWeight}`);
        console.log("========================================================");

        console.log("========================================================");
        console.log("Close voting:");
        await voteContractDeploy.closeVoting();
        console.log(`Is vote open? - ${await voteContractDeploy.isVoteOpen()}`);
        console.log("========================================================");

        console.log("========================================================");
        console.log("Winning second proposal!");
        console.log(`Second proposal vote count: ${(await voteContractDeploy.getWinningProposals())[0].totalVoteWeight}`);
        console.log(`Second proposal command: ${(await voteContractDeploy.getWinningProposals())[0].commandName}`);
        console.log(`Second proposal command owner: ${(await voteContractDeploy.getWinningProposals())[0].ownerAddress}`);
        console.log("========================================================");

        console.log("========================================================");
        console.log("Owner from winnig proposal added to gnosis!");
        console.log(await gnosis.getOwners());
        console.log("========================================================");
      });
    }
  );
});
