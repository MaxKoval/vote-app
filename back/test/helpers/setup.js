const tokens = require("./tokens");
const { constants } = require("@openzeppelin/test-helpers");
const ConfigurableRightsPool = artifacts.require("ConfigurableRightsPool");
const { toWei } = require("web3-utils");

const MAX = web3.utils.toTwosComplement(-1);

const CREATE_OWNER_COMMAND = "createOwner";
const REMOVE_OWNER_COMMAND = "removeOwner";

const initialize = async () => {
  const setup = {};

  const accounts = await ethers.getSigners();

  const proposals = [
    {
      totalVoteWeight: 0,
      commandName: CREATE_OWNER_COMMAND,
      ownerAddress: accounts[1].address,
    },
    {
      totalVoteWeight: 0,
      commandName: CREATE_OWNER_COMMAND,
      ownerAddress: accounts[2].address,
    },
    {
      totalVoteWeight: 0,
      commandName: CREATE_OWNER_COMMAND,
      ownerAddress: accounts[3].address,
    },
    {
      totalVoteWeight: 0,
      commandName: CREATE_OWNER_COMMAND,
      ownerAddress: accounts[4].address,
    },
  ];

  const proposalWithEmptyName = [
    {
      totalVoteWeight: 0,
      commandName: CREATE_OWNER_COMMAND,
      ownerAddress: accounts[5].address,
    },
  ];

  const proposalWithEmptyCommand = [
    {
      totalVoteWeight: 0,
      commandName: "",
      ownerAddress: accounts[6].address,
    },
  ];

  const proposalWithEmptyOwnerAddress = [
    {
      totalVoteWeight: 0,
      commandName: "command",
      ownerAddress: constants.ZERO_ADDRESS,
    },
  ];

  const duplacatesProposals = [
    {
      totalVoteWeight: 0,
      commandName: CREATE_OWNER_COMMAND,
      ownerAddress: accounts[7].address,
    },
    {
      totalVoteWeight: 0,
      commandName: CREATE_OWNER_COMMAND,
      ownerAddress: accounts[7].address,
    },
  ];

  const proposalWithExistedOwner = [
    {
      totalVoteWeight: 0,
      commandName: CREATE_OWNER_COMMAND,
      ownerAddress: accounts[9].address,
    },
  ];

  const proposalWithUnexistedOwner = [
    {
      totalVoteWeight: 0,
      commandName: REMOVE_OWNER_COMMAND,
      ownerAddress: accounts[10].address,
    },
  ];

  const proposalToRemoveOwner = [
    {
      totalVoteWeight: 0,
      commandName: REMOVE_OWNER_COMMAND,
      ownerAddress: accounts[11].address,
    },
  ];

  setup.tokens = await tokens.getErc20TokenInstances(2, setup.root);

  setup.accounts = accounts;

  setup.proposals = proposals;
  setup.proposalWithEmptyName = proposalWithEmptyName;
  setup.proposalWithEmptyCommand = proposalWithEmptyCommand;
  setup.proposalWithEmptyOwnerAddress = proposalWithEmptyOwnerAddress;
  setup.duplacatesProposals = duplacatesProposals;
  setup.proposalWithExistedOwner = proposalWithExistedOwner;
  setup.proposalWithUnexistedOwner = proposalWithUnexistedOwner;
  setup.proposalToRemoveOwner = proposalToRemoveOwner;

  return setup;
};

const createGnosis = async (ownerAddress, threshold) => {
  const gnosisFactory = await ethers.getContractFactory("GnosisOwnerManager");
  const gnosis = await gnosisFactory.deploy();
  await gnosis.setupOwners([ownerAddress], threshold);

  return gnosis;
};

const createBalancer = async (tokens) => {
  // to work around an error when running a test coverage check
  const BalancerSafeMath = artifacts.require("BalancerSafeMath");
  const RightsManager = artifacts.require("RightsManager");
  const SmartPoolManager = artifacts.require("SmartPoolManager");
  const BFactory = artifacts.require("BFactory");
  const CRPFactory = artifacts.require("CRPFactory");

  const bfactory = await BFactory.new();
  const balancerSafeMath = await BalancerSafeMath.new();
  const rightsManager = await RightsManager.new();
  const smartPoolManager = await SmartPoolManager.new();

  await CRPFactory.link(balancerSafeMath, balancerSafeMath.address);
  await CRPFactory.link(rightsManager, rightsManager.address);
  await CRPFactory.link(smartPoolManager, smartPoolManager.address);

  const crpFactory = await CRPFactory.new();

  const usdc = tokens[0];
  const dai = tokens[1];

  const USDC = await usdc.address;
  const DAI = await dai.address;

  const tokenAddresses = [DAI, USDC];

  const swapFee = 10 ** 15;
  const startWeights = [toWei("8"), toWei("1")];
  const startBalances = [toWei("10000"), toWei("5000")];
  const SYMBOL = "BPOOL";
  const NAME = "Prime Balancer Pool Token";

  const permissions = {
    canPauseSwapping: true,
    canChangeSwapFee: true,
    canChangeWeights: true,
    canAddRemoveTokens: true,
    canWhitelistLPs: false,
  };

  const poolParams = {
    poolTokenSymbol: SYMBOL,
    poolTokenName: NAME,
    constituentTokens: tokenAddresses,
    tokenBalances: startBalances,
    tokenWeights: startWeights,
    swapFee: swapFee,
  };

  POOL = await crpFactory.newCrp.call(
    bfactory.address,
    poolParams,
    permissions
  );

  await crpFactory.newCrp(bfactory.address, poolParams, permissions);

  const pool = await ConfigurableRightsPool.at(POOL);

  await usdc.approve(POOL, MAX);
  await dai.approve(POOL, MAX);

  await pool.createPool(toWei("1000"), 10, 10);

  return pool;
};

module.exports = {
  initialize,
  createGnosis,
  createBalancer
};
